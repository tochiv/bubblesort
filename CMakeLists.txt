cmake_minimum_required(VERSION 3.25)
project(OPPR)

set(CMAKE_CXX_STANDARD 14)

add_executable(OPPR
        quickSort.cpp)
